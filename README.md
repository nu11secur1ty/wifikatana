***wifikatana _kali_linux_ 2017.1_ - deprecated***

# wificatana _kali_Linux_ 2019 VERSION="2019.1"
 - code name ***damaskina***
# wifi penetration testing tool from nu11secur1ty
![](https://github.com/nu11secur1ty/wifikatana/blob/master/cover/old_katana_by_jigglyritz-d9cq65h.png)
For more information how to use, please visit

# link:
```
http://www.nu11secur1ty.com/2014/10/attack-wps-wifi-protected-wpa-wpa2script.html
```

# Download:
``` 
git clone https://github.com/nu11secur1ty/wifikatana.git
```

